
# Git Cheat-Sheet

Some useful commands.

## miscellaneous

- git init

## check the current status:

- git status
- git log
- git log --all --decorate --oneline --graph

## stage / unstage

- git add <filename>
- git rm —cached <filename>

## work with commits

- git commit -m "my message goes here"
- git checkout <branch_name/commit_hash>
- git checkout . # checks out the commit that HEAD points to

#!/usr/bin/python3


import socket


HOST = "0.0.0.0" # socket.gethostname()
PORT = 53007
MAX_REQUESTS = 10
MAX_BYTES_READ = 1024


user = {"192.168.1.142": "testuser1",
        "192.168.1.245": "testuser2",
        "192.168.1.146": "testuser3"}


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((HOST, PORT))
serversocket.listen(MAX_REQUESTS)


while True:
    clientsocket,addr = serversocket.accept()

    # print('Got a connection from %s' % str(addr))

    msg = clientsocket.recv(MAX_BYTES_READ)
    ip = str(addr[0])
    if ip in user:
        print("From: " + user[ip] + " '" + msg.decode("utf-8") + "'")
    else:
        print("From: " + ip + " '" + msg.decode("utf-8") + "'")

    msg = "Thank you for connecting" + "\r\n"
    clientsocket.send(msg.encode('utf-8'))
    clientsocket.close()
